==================================
 GitHub script for pull requests
==================================


Use it script for make pull requests for all repos all branches
---------------------------------------------------------------

* Add a new SSH key to your GitHub account. See: https://help.github.com/en/articles/adding-a-new-ssh-key-to-your-github-account
* Create new <your file> in your work directory, copy-paste and update this code:

.. code-block:: console

    #!/bin/bash

	upstreamUrlGit=https://github.com/<your upstream git name>
    originUrlGit=https://github.com/<your origin git name>
    directoryClone=$HOME/<your>/<work>/<directory>
    username=<your origin git name>
    mess=$"<your message for commit>"

    #list repos
	modules=(
		<your upstream repo 1>
		<your upstream repo 2>
		<your upstream repo 3>
	)

    # list branches
	branches=(
		<your branch 1>
		<your branch 2>
		<your branch 3>
	)

    for j in "${modules[@]}"; do
        git clone $upstreamUrlGit/$j.git $directoryClone/$j
        cd $directoryClone/$j
        git remote rename origin upstream
        git remote add origin $originUrlGit/$j.git
        git remote set-url origin git@github.com:$username/$j.git
        for i in "${branches[@]}"; do
            git branch $i upstream/$i
            git checkout $i
            # check file. For example:
		    if grep -qx '    on_failure: change' .travis.yml
            then
                echo "Hola!"
            else
                # add new strings to file. For example:
		        { echo '  webhooks:'; echo '    on_failure: change'; echo '  urls:'; echo '    - "https://ci.it-projects.info/travis/on_failure/change"';} >> ./.travis.yml
                git add .travis.yml
                git commit -m "$mess"
                git push origin -f $i
                hub pull-request -b <your upstream git name>:$i -m "$mess"
            fi
        done
    done


* Install hub. Look this: https://odoo-development.readthedocs.io/en/latest/git/utils.html 
* Run your file in terminal from your work directory ``./<your file>``

